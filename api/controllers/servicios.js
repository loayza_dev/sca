const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const invoke = require('../helpers/invoke');
const query = require('../helpers/query');

// POST Servicios
exports.registrarServicio = asyncHandler(async (req, res, next) => {
  try {
    var chaincodeName = req.params.chaincodeName;
    var channelName = req.params.channelName;
    var fcn = req.body.fcn;
    var args = req.body.args;
    console.log('channelName  : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn  : ' + fcn);
    console.log('args  : ' + args);

    let message = await invoke.invokeTransaction(
      channelName,
      chaincodeName,
      fcn,
      args,
      'Org1'
    );
    console.log(`message result is : ${message}`);

    const response_payload = {
      result: message,
      error: null,
      errorData: null,
    };

    res.status(201).json({ success: true, data: response_payload });
  } catch (error) {
    const response_payload = {
      result: null,
      error: error.name,
      errorData: error.message,
    };
    res.status(500).json({ success: false, data: response_payload });
  }
});

// Get servicios
exports.obtenerServicios = asyncHandler(async (req, res, next) => {
  try {
    console.log('==================== QUERY BY CHAINCODE ==================');

    var channelName = req.params.channelName;
    var chaincodeName = req.params.chaincodeName;
    let args = req.query.args;
    let fcn = req.query.fcn;

    console.log('channelName : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn : ' + fcn);
    console.log('args : ' + args);

    console.log('args==========', args);
    args = args.replace(/'/g, '"');
    args = JSON.parse(args);

    let message = await query.query(
      channelName,
      chaincodeName,
      args,
      fcn,
      'Org1'
    );

    const response_payload = {
      result: message,
      error: null,
      errorData: null,
    };

    res.status(200).json({ success: true, data: response_payload });
  } catch (error) {
    const response_payload = {
      result: null,
      error: error.name,
      errorData: error.message,
    };
    res.send(response_payload);
  }
});
