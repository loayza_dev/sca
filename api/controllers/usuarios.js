const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const invoke = require('../helpers/invoke');
const query = require('../helpers/query');

// Peticion post para registrar usuarios
exports.registrarUsuario = asyncHandler(async (req, res, next) => {
  try {
    var chaincodeName = req.params.chaincodeName;
    var channelName = req.params.channelName;
    var fcn = req.body.fcn;
    var args = req.body.args;
    console.log('channelName  : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn  : ' + fcn);
    console.log('args  : ' + args);

    let message = await invoke.invokeTransaction(
      channelName,
      chaincodeName,
      fcn,
      args,
      'Org1'
    );
    console.log(`message result is : ${message}`);

    const response_payload = {
      result: message,
      error: null,
      errorData: null,
    };

    res.status(201).json({ success: true, data: response_payload });
  } catch (error) {
    const response_payload = {
      result: null,
      error: error.name,
      errorData: error.message,
    };
    res.status(500).json({ success: false, data: response_payload });
  }
});

// Obtener informacion del Usuario
exports.obtenerUsuario = asyncHandler(async (req, res, next) => {
  try {
    console.log('==================== OBTENER USUARIO ==================');

    var channelName = req.params.channelName;
    var chaincodeName = req.params.chaincodeName;
    let args = req.query.args;
    let fcn = req.query.fcn;

    console.log('channelName : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('fcn : ' + fcn);
    console.log('args : ' + args);

    console.log('args==========', args);
    args = args.replace(/'/g, '"');

    args = JSON.parse(args);

    let message = await query.query(
      channelName,
      chaincodeName,
      args,
      fcn,
      'Org1'
    );

    const response_payload = {
      result: message,
      error: null,
      errorData: null,
    };

    res.status(200).json({ success: true, data: response_payload });
  } catch (error) {
    const response_payload = {
      result: null,
      error: error.name,
      errorData: error.message,
    };
    res.send(response_payload);
  }
});

// Funcion de verificacion de autorizacion
exports.verificarAutorizacion = asyncHandler(async (req, res, next) => {
  try {
    console.log(
      '==================== VERIFICACION DE AUTORIZACION =================='
    );

    var channelName = req.params.channelName;
    var chaincodeName = req.params.chaincodeName;
    let args = req.query.args;

    console.log('channelName : ' + channelName);
    console.log('chaincodeName : ' + chaincodeName);
    console.log('args : ' + args);

    if (!chaincodeName || !channelName || !args) {
      res.status(400).json({
        success: false,
        data: { result: null, error: 'Bad request.' },
      });
      return;
    }

    args = args.replace(/'/g, '"');
    args = JSON.parse(args);

    let usuario = await query.query(
      channelName,
      chaincodeName,
      [args[0]],
      'ObtenerUsuario',
      'Org1'
    );
    let servicio = await query.query(
      channelName,
      chaincodeName,
      [args[1]],
      'ObtenerServicio',
      'Org1'
    );
    let rol = await query.query(
      channelName,
      chaincodeName,
      [usuario.rol],
      'ObtenerRol',
      'Org1'
    );
    let operaciones = servicio.operaciones.split(',');
    // console.log('Rol>>>>>>>>>>', rol);
    // console.log('usuario>>>>>>>>>>', usuario);
    // console.log('servicio>>>>>>>>>>', servicio);
    // console.log('Operaciones>>>>>>>>>>', operaciones);

    let response = { operacion: args[2], servicio: args[1], autorizado: false };

    if (
      rol.estado === 'activo' &&
      usuario.rol === servicio.rol &&
      operaciones.includes(args[2])
    ) {
      response.autorizado = true;
    }

    res.status(200).json({
      success: true,
      data: { result: response, error: null, errorData: null },
    });
  } catch (error) {
    const response_payload = {
      result: null,
      error: error.name,
      errorData: error.message,
    };
    res.status(500).json({ success: false, data: { response_payload } });
  }
});
