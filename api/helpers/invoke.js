const { Gateway, Wallets } = require('fabric-network');

const helper = require('./helper');

const invokeTransaction = async (
  channelName,
  chaincodeName,
  fcn,
  args,
  org_name
) => {
  try {
    const ccp = await helper.getCCP(org_name);

    const walletPath = await helper.getWalletPath(org_name);
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    console.log(`Wallet path: ${walletPath}`);

    let identity = await wallet.get('admin');
    if (!identity) {
      console.log(
        'La identidad para el administrador no existe en el wallet -> Registrando Administrador'
      );

      await helper.enrollAdmin(org_name);
      identity = await wallet.get('admin');
    }

    const connectOptions = {
      wallet,
      identity: 'admin',
      discovery: { enabled: true, asLocalhost: true },
    };

    const gateway = new Gateway();
    await gateway.connect(ccp, connectOptions);

    const network = await gateway.getNetwork(channelName);
    const contract = network.getContract(chaincodeName);

    // Multiple smartcontract in one chaincode
    let result;
    let message;

    switch (fcn) {
      case 'RegistrarRol':
        result = await contract.submitTransaction(
          'RolContract:' + fcn,
          args[0],
          args[1],
          args[2],
          args[3],
          args[4]
        );
        result = { txid: result.toString() };
        break;
      case 'RegistarServicio':
        result = await contract.submitTransaction(
          'ServicioContract:' + fcn,
          args[0],
          args[1],
          args[2],
          args[3],
          args[4],
          args[5],
          args[6]
        );
        result = { txid: result.toString() };
        break;
      case 'RegistrarUsuario':
        result = await contract.submitTransaction(
          'UsuarioContract:' + fcn,
          args[0],
          args[1],
          args[2]
        );
        result = { txid: result.toString() };
        break;
      case 'CreateCar':
        result = await contract.submitTransaction(
          'SmartContract:' + fcn,
          args[0]
        );
        result = { txid: result.toString() };
        break;
      case 'UpdateCarOwner':
        console.log('=============');
        result = await contract.submitTransaction(
          'SmartContract:' + fcn,
          args[0],
          args[1]
        );
        result = { txid: result.toString() };
        break;
      case 'CreateDocument':
        result = await contract.submitTransaction(
          'DocumentContract:' + fcn,
          args[0]
        );
        console.log(result.toString());
        result = { txid: result.toString() };
        break;
      default:
        break;
    }

    await gateway.disconnect();

    // result = JSON.parse(result.toString());

    let response = {
      message: message,
      result,
    };

    return response;
  } catch (error) {
    console.log(`Getting error: ${error}`);
    return error.message;
  }
};

exports.invokeTransaction = invokeTransaction;
