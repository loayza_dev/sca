import http from 'k6/http';
import { check } from 'k6';
import { Rate } from 'k6/metrics';

export let errorRate = new Rate('errors');

export let options = {
  vus: 10,
  duration: '30s',
};

export default function () {
  var url =
    'http://localhost:5000/api/v1/usuarios/channels/universidad/chaincodes/sca_cc/autorizacion?args=["ux","sx","GET"]';

  check(http.get(url), {
    'Código de estado es 200': (r) => r.status == 200,
  }) || errorRate.add(1);
}
