import http from 'k6/http';
import { check, sleep } from 'k6';
import { Rate } from 'k6/metrics';
export let errorRate = new Rate('errors');

export let options = {
  vus: 10,
  duration: '30s',
};

export default function () {
  var url =
    'http://localhost:5000/api/v1/servicios/channels/universidad/chaincodes/sca_cc';

  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  var payload = JSON.stringify({
    fcn: 'RegistarServicio',
    chaincodeName: 'sca_cc',
    channelName: 'universidad',
    args: [
      's1',
      '/informacion/organizacion/org1',
      'GET,PUT',
      'r1',
      'Org1',
      'Descripcion de ejemplo',
      'activo',
    ],
  });

  check(http.post(url, payload, params), {
    'Código de estado es 201': (r) => r.status == 201,
  }) || errorRate.add(1);

  sleep(1);
}
