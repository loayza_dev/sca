
#Generar el material criptografico para las organizaciones
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/


# Canal del sistema
SYS_CHANNEL="sys-channel"

# Nombre del canal
CHANNEL_NAME="universidad"

echo $CHANNEL_NAME

# Generar el Genesis block del sistema
configtxgen -profile ThreeOrgsOrdererGenesis -configPath . -channelID $SYS_CHANNEL  -outputBlock ../channel-artifacts/genesis.block


# Generar al bloque de configuración del canal
configtxgen -profile ThreeOrgsChannel -configPath . -outputCreateChannelTx ../channel-artifacts/channel.tx -channelID $CHANNEL_NAME

echo "#######    Generando anchor peer update para Org1MSP  ##########"
configtxgen -profile ThreeOrgsChannel -configPath . -outputAnchorPeersUpdate ../channel-artifacts/Org1MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org1MSP

echo "#######    Generando anchor peer update para Org2MSP  ##########"
configtxgen -profile ThreeOrgsChannel -configPath . -outputAnchorPeersUpdate ../channel-artifacts/Org2MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org2MSP

echo "#######    Generando anchor peer update para Org3MSP  ##########"
configtxgen -profile ThreeOrgsChannel -configPath . -outputAnchorPeersUpdate ../channel-artifacts/Org3MSPanchors.tx -channelID $CHANNEL_NAME -asOrg Org3MSP